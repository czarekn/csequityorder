# CSEquityOrder

The project implements the requirements with the following comments or presumptions:

* OrderPlacedEvent - might be missing quantity

* Unclear what to do if a partial buy might be done/was done - this I guess depends on  the specific implementation of the IOrderService and requirements for the EquityOrder implementation.
It's possible to imagine that the instance stays active until it's fully filled with few partial buys.

* OrderService should be throwing specific exception types to handle (this was demonstrated in the UnitTests)

* thred safety - no specific requirements were given, especially for the event handlers thread safety - see below comments


## Branches
* **master** - contains base implementation with thread safety implemented for buying and the component being active or not after first successful buy or failure
* **threadsafe** - contains the implementation of the thread safe version for event handlers (ading/removing event handlers, handling the events execution)

## Thread safety
The approach to thread safety for event handlers and events may very depending on adopted strategy. Some claim best would be keep the events not thread safe but 
make sure the events are subsribed and handled on the same thread always. 
Some of the thread safe approaches vary, depending on the requirements related to race conditions, calling unsubsribed handlers, how much of the safety can be delegated
to the subsribers, etc. Please, see comments below and references used.
### on master branch
On master branch, the component is threadsafe for ReceiveTick being called from multiple threads and controlling the active state of the instance (first successful buy or first failure). 
### on threadsafe branch
There were no specific requirements given related to thread safety of the event handling part. That may vary, depending on the adopted strategy. 
This implementation solves most of the issues, with the presumption that the event handlers will not call the code to subscribe to or unsubscribe from events.
Good analysis of possible options for eventing thread safety is given in the following articles:
https://www.codeproject.com/Articles/886223/Csharp-Multithreading-and-Events
https://www.codeproject.com/Articles/37474/Threadsafe-Events
