﻿using System;
using CSEquityOrder;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NMock;

namespace CSEquityOrderTests
{
    [TestClass]
    public class EquityOrderUnitTests
    {
        private MockFactory _factory;
        private Mock<IOrderService> _orderServiceMock;

        private OrderPlacedEventArgs _orderPlacedEventArgs;
        private OrderErroredEventArgs _orderErroredEventArgs;

        [TestInitialize]
        public void Initialize()
        {
            _factory = new MockFactory();
            _orderServiceMock = _factory.CreateMock<IOrderService>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            _factory.VerifyAllExpectationsHaveBeenMet();
            _factory.ClearExpectations();
        }

        #region Constructor
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_OrderServiceNull_ThrowsException()
        {
            var equityOrder = new EquityOrder(null, "AA", 10, 12.0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_EquitySymbolEmpty_ThrowsException()
        {
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "", 10, 12.0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_EquitySymbolNull_ThrowsException()
        {
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, null, 10, 12.0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_QuantityNotPositive_ThrowsException()
        {
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 0, 10.0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_QuantityNegative_ThrowsException()
        {
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", -10, 10.0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_PriceNotPositive_ThrowsException()
        {
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 0.0m);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_PriceNegative_ThrowsException()
        {
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, -10.0m);
        }
        #endregion

        [TestMethod]
        public void Buy_EquityCodeMatchesNotAndPriceIsGood_CallsNotBuyServiceAndNoEventsCalled()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            _orderServiceMock
                .Expects
                .No.Method(os => os.Buy(null, 0, 0m));
            equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder.OrderErrored += OrderErroredEventHandlerImpl;

            // Act
            equityOrder.ReceiveTick("BB", 11.5m);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);
        }

        [TestMethod]
        public void Buy_EquityCodeMatchesAndPriceIsNotGood_CallsNotBuyServiceAndNoEventsCalled()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            _orderServiceMock
                .Expects
                .No.Method(os => os.Buy(null, 0, 0m));
            equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder.OrderErrored += OrderErroredEventHandlerImpl;

            // Act
            equityOrder.ReceiveTick("AA", 12.0m);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);
        }

        [TestMethod]
        public void Buy_EquityCodeMatchesAndPriceIsGood_CallsBuyService()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 11.5m))
                .With("AA", 10, 11.5m);

            // Act
            equityOrder.ReceiveTick("AA", 11.5m);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
        }

        [TestMethod]
        public void Buy_BuySuccessful_CallsOrderPlacedEventHandler()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 11.5m))
                .With("AA", 10, 11.5m);
            equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder.OrderErrored += OrderErroredEventHandlerImpl;

            // Act
            equityOrder.ReceiveTick("AA", 11.5m);

            // Assert
            Assert.IsNotNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);
            Assert.AreEqual("AA", _orderPlacedEventArgs.EquityCode);
            Assert.AreEqual(11.5m, _orderPlacedEventArgs.Price);
        }

        [TestMethod]
        public void Buy_BuysOnlyOnce()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 11.5m))
                .With("AA", 10, 11.5m);
            equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder.OrderErrored += OrderErroredEventHandlerImpl;

            // Act
            equityOrder.ReceiveTick("AA", 11.5m);
            _orderPlacedEventArgs = null;
            equityOrder.ReceiveTick("AA", 11.5m);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);
        }

        [TestMethod]
        public void Buy_WhenBuyFails_CallsOrderErroredEventHandler()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            var exceptionObject = new OrderServiceBuyException();
            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 11.5m))
                .With("AA", 10, 11.5m)
                .Will(Throw.Exception(exceptionObject));
            equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder.OrderErrored += OrderErroredEventHandlerImpl;

            // Act
            equityOrder.ReceiveTick("AA", 11.5m);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNotNull(_orderErroredEventArgs);
            Assert.AreSame(exceptionObject, _orderErroredEventArgs.GetException());
            Assert.AreEqual("AA", _orderErroredEventArgs.EquityCode);
            Assert.AreEqual(11.5m, _orderErroredEventArgs.Price);
        }

        [TestMethod]
        public void Buy_WhenBuyFails_WillNotTryToBuyAgain()
        {
            // Arrange
            var equityOrder = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            var exceptionObject = new OrderServiceBuyException();
            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 11.5m))
                .With("AA", 10, 11.5m)
                .Will(Throw.Exception(exceptionObject));
            equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder.OrderErrored += OrderErroredEventHandlerImpl;

            // Act
            equityOrder.ReceiveTick("AA", 11.5m);
            _orderErroredEventArgs = null;
            equityOrder.ReceiveTick("AA", 11.5m);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);
        }

        [TestMethod]
        public void Buy_MultipleInstances_EachInstanceBuysOnce()
        {
            // Arrange
            var equityOrder1 = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 12.0m);
            var equityOrder2 = new EquityOrder(_orderServiceMock.MockObject, "AA", 10, 14.0m);

            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 11.5m))
                .With("AA", 10, 11.5m);

            _orderServiceMock
                .Expects
                .One.Method(os => os.Buy("AA", 10, 13.5m))
                .With("AA", 10, 13.5m);

            equityOrder1.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder1.OrderErrored += OrderErroredEventHandlerImpl;
            equityOrder2.OrderPlaced += OrderPlacedEventHandlerImpl;
            equityOrder2.OrderErrored += OrderErroredEventHandlerImpl;

            equityOrder1.ReceiveTick("AA", 11.5m);
            equityOrder2.ReceiveTick("AA", 13.5m);
            _orderPlacedEventArgs = null;

            // Act
            equityOrder1.ReceiveTick("AA", 11.5m);
            // Assert
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);

            // Act
            equityOrder2.ReceiveTick("AA", 13.5m);
            // Assert
            Assert.IsNull(_orderPlacedEventArgs);
            Assert.IsNull(_orderErroredEventArgs);

            // Assert
            _factory.VerifyAllExpectationsHaveBeenMet();
        }

        private class OrderServiceBuyException : Exception
        {
        }

        private void OrderPlacedEventHandlerImpl(OrderPlacedEventArgs args)
        {
            _orderPlacedEventArgs = args;
        }

        private void OrderErroredEventHandlerImpl(OrderErroredEventArgs args)
        {
            _orderErroredEventArgs = args;
        }
    }
}
