﻿using CSEquityOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSEquityOrderConsoleDemoApp
{
    class OrderServiceImpl : IOrderService
    {
        public void Buy(string equityCode, int quantity, decimal price)
        {
            if (price > 100m)
                throw new InvalidOperationException();
        }

        public void Sell(string equityCode, int quantity, decimal price)
        {
            throw new NotImplementedException();
        }
    }
}
