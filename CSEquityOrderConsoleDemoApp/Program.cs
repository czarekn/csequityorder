﻿using CSEquityOrder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSEquityOrderConsoleDemoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var orderService = new OrderServiceImpl();

            

            var range = Enumerable.Range(85, 30);
            foreach (var price in range)
            {
                Console.WriteLine($"New EquityOrder");
                EquityOrder equityOrder = new EquityOrder(orderService, "AA", 99, 110m);
                equityOrder.OrderPlaced += OrderPlacedEventHandlerImpl;
                equityOrder.OrderErrored += OrderErroredEventHandlerImpl;
                Console.WriteLine($"Sending tick for: {price}");
                equityOrder.ReceiveTick("AA", price);
                Console.WriteLine($"Sending tick for: {price+1}");
                equityOrder.ReceiveTick("AA", price + 1);
                Console.WriteLine();
            }

            Console.WriteLine("Done.");
            Console.ReadLine();
        }

        private static void OrderPlacedEventHandlerImpl(OrderPlacedEventArgs args)
        {
            Console.WriteLine($"Order placed for: {args.EquityCode}, {args.Price}");
        }

        private static void OrderErroredEventHandlerImpl(OrderErroredEventArgs args)
        {
            Console.WriteLine($"Order placed for: {args.EquityCode}, {args.Price}, {args.GetException().Message}");
        }
    }
}
