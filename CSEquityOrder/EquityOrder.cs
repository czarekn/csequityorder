﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSEquityOrder
{
    public class EquityOrder : IEquityOrder
    {
        public event OrderPlacedEventHandler OrderPlaced;
        public event OrderErroredEventHandler OrderErrored;

        private IOrderService m_orderService;
        private string m_equityCode;
        private int m_quantity;
        private decimal m_price;

        private bool m_active = true;

        private object m_syncActive = new object();

        public EquityOrder(IOrderService orderService, string equityCode, int quantity, decimal price)
        {
            if (orderService == null)
                throw new ArgumentNullException("Order service is null", nameof(orderService));
            if (string.IsNullOrEmpty(equityCode))
                throw new ArgumentException("Equity code empty", nameof(equityCode));
            if (quantity <= 0)
                throw new ArgumentException("Price not positive value", nameof(quantity));
            if (price <= 0.0m)
                throw new ArgumentException("Price not positive value", nameof(price));

            m_orderService = orderService;
            m_equityCode = equityCode;
            m_quantity = quantity;
            m_price = price;
        }

        public void ReceiveTick(string equityCode, decimal price)
        {
            lock (m_syncActive)
            {
                if (!m_active)
                    return;

                try
                {
                    if (m_equityCode.Equals(equityCode)
                        && m_price > price)
                    {
                        m_orderService.Buy(m_equityCode, m_quantity, price);
                        OrderPlaced?.Invoke(new OrderPlacedEventArgs(m_equityCode, price));
                        m_active = false;
                    }
                }
                catch (Exception e) // this should be specific OrderService exception type
                {
                    OrderErrored?.Invoke(new OrderErroredEventArgs(m_equityCode, price, e));
                    m_active = false;
                }
            }
        }
    }
}
